import {Component, OnInit} from '@angular/core';
import {NewsService} from '../../../service/news.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Category} from '../../../model/category';
import {News} from '../../../model/news';
import {CategoryService} from '../../../service/category.service';
import {NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer} from '@angular/platform-browser';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  categorys: Category[];
  currentCategory: any;
  news: News[];
  category: Category = new Category();
  id: any;
  config: any;

  // tslint:disable-next-line:max-line-length
  constructor(private sanitizer: DomSanitizer, private categoryServie: CategoryService, private service: NewsService,  config1: NgbModalConfig, private router: Router, private route: ActivatedRoute) {
    config1.backdrop = 'static';
    config1.keyboard = false;
    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.news
    };
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  ngOnInit() {
    this.getAll();
    this.route.queryParams.subscribe(param => {
      this.id = param.id;
    });
    this.service.getNewsByCategory(this.id).subscribe(res => {
      this.news = res.data;
    });
  }

  getAll() {
    this.categoryServie.list().subscribe(res => {
      this.categorys = res.data;
    });
  }

  // goNews() {
  //   // localStorage.setItem('currentCategory', this.id);
  //   this.router.navigate(['newsUniversity']);
  // }

}
