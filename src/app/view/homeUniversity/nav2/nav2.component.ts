import {Component, OnInit} from '@angular/core';
import {Category} from '../../../model/category';
import {CategoryService} from '../../../service/category.service';
import {NewsService} from '../../../service/news.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav2',
  templateUrl: './nav2.component.html',
  styleUrls: ['./nav2.component.css']
})
export class Nav2Component implements OnInit {
  categorys: Category[];

  // tslint:disable-next-line:max-line-length
  constructor(private categoryServie: CategoryService, private newsService: NewsService, private router: Router) {
  }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.categoryServie.list().subscribe(res => {
      this.categorys = res.data;
    });
  }
}

