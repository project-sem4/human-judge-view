import {Component, OnInit} from '@angular/core';
import {Category} from '../../../model/category';
import {CategoryService} from '../../../service/category.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NewsService} from '../../../service/news.service';
import {News} from '../../../model/news';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  categorys: Category[];

  // tslint:disable-next-line:max-line-length
  constructor(private categoryServie: CategoryService, private newsService: NewsService, private router: Router) {
  }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.categoryServie.list().subscribe(res => {
      this.categorys = res.data;
    });
  }
}
