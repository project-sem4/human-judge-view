import {Component, OnInit, TemplateRef} from '@angular/core';
import {Router} from '@angular/router';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {Note} from '../../../model/note';
import {NoteService} from '../../../service/note.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-note',
  templateUrl: './list-note.component.html',
  styleUrls: ['./list-note.component.css']
})
export class ListNoteComponent implements OnInit {
  searchText;
  config: any;
  notes: Note[];
  note: Note = new Note();
  modalRef: BsModalRef;
  id: any;

  // tslint:disable-next-line:max-line-length
  constructor(private service: NoteService, private router: Router, config1: NgbModalConfig, private modalService: NgbModal,  private modalService2: BsModalService) {
    config1.backdrop = 'static';
    config1.keyboard = false;
    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.notes
    };
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  ngOnInit() {
    const id = localStorage.getItem('id');
    this.service.getNote(id).subscribe(res => {
      this.note = res.data;
    });
    this.getAll();
  }

  getAll() {
    this.service.list().subscribe(res => {
      this.notes = res.data;
    });
  }

  delete(id: string) {
    Swal.fire({
      title: 'Bạn chắc chắn xóa ghi chú này?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Đồng ý!',
      cancelButtonText: 'Thoát'
    }).then((result) => {
      if (result.value) {
        this.service.delete(id).subscribe(res => {
          Swal.fire({
              icon: 'success',
              title: 'Đã xóa thành công',
              text: '',
              showConfirmButton: false,
              timer: 1000
            }
          );
          this.getAll();
          }
        );
      }
    });
  }

  openModal(id: any, template: TemplateRef<any>) {
    this.service.getNote(id).subscribe(res => {
      this.note = res.data;
    });
    this.modalRef = this.modalService2.show(template);
    this.id = id;
  }

  submitEdit() {
    this.modalRef.hide();
    this.service.update(this.note.id, this.note).subscribe( res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1500
      });
      this.router.navigate(['note']);
      }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
      }
    );
    window.location.reload();
  }
}
