import {Component, OnInit} from '@angular/core';
import {SubjectService} from '../../../service/subject.service';
import {Router} from '@angular/router';
import {Subject} from '../../../model/subject';
import {SemesterService} from '../../../service/semester.service';
import {Semester} from '../../../model/semester';
import Swal from 'sweetalert2';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-subject',
  templateUrl: './add-subject.component.html',
  styleUrls: ['./add-subject.component.css']
})
export class AddSubjectComponent implements OnInit {
  subject: Subject = new Subject();
  semesters: Semester[];
  semester: Semester = new Semester();
  registerForm: FormGroup;
  submitted = false;

  // tslint:disable-next-line:max-line-length
  constructor(private service: SubjectService, private semesterService: SemesterService, private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.semesterService.list().subscribe(res => {
      this.semesters = res.data;
    });
    this.registerForm = this.formBuilder.group({
      code: ['', Validators.required],
      subject: [this.subject.subject, Validators.compose([Validators.required, Validators.minLength(10)])],
      period: ['', Validators.required],
      semesterId: ['', Validators.required]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.service.create(this.subject, this.subject.semesterId).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.router.navigate(['listSubject']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }
}
