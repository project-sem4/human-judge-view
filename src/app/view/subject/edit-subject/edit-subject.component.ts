import {Component, OnInit} from '@angular/core';
import {Subject} from '../../../model/subject';
import {SubjectService} from '../../../service/subject.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Semester} from '../../../model/semester';
import {SemesterService} from '../../../service/semester.service';
import Swal from 'sweetalert2';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-edit-subject',
  templateUrl: './edit-subject.component.html',
  styleUrls: ['./edit-subject.component.css']
})
export class EditSubjectComponent implements OnInit {
  subject: Subject = new Subject();
  semesters: Semester[];
  semester: Semester = new Semester();
  id: any;
  registerForm: FormGroup;
  submitted = false;

  // tslint:disable-next-line:max-line-length
  constructor(private service: SubjectService, private semesterService: SemesterService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(param => {
      this.id = param.id;
    });
    this.service.getSubject(this.id).subscribe(res => {
      this.subject = res.data;
    });
    this.semesterService.list().subscribe(res => {
      this.semesters = res.data;
    });
    this.registerForm = this.formBuilder.group({
      code: ['', Validators.required],
      subject: [this.subject.subject, Validators.compose([Validators.required, Validators.minLength(10)])],
      period: ['', Validators.required],
      semesterId: ['', Validators.required]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onEdit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.service.update(this.id, this.subject.semesterId, this.subject).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Bạn đã lưu thành công',
          showConfirmButton: false,
          timer: 1000
        });
        this.router.navigate(['listSubject']);
      }, error => {
        Swal.fire({
          icon: 'error',
          title: '',
          text: 'Một vài lỗi đã sảy ra!'
        });
      }
    );
  }
}
