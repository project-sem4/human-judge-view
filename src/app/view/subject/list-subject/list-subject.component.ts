import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {Subject} from '../../../model/subject';
import {SubjectService} from '../../../service/subject.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-subject',
  templateUrl: './list-subject.component.html',
  styleUrls: ['./list-subject.component.css']
})
export class ListSubjectComponent implements OnInit {
  searchText;
  config: any;
  subjects: Subject[];
  subject: Subject = new Subject();
  closeResult: string;

  constructor(private service: SubjectService, private router: Router, config1: NgbModalConfig, private modalService: NgbModal) {
    config1.backdrop = 'static';
    config1.keyboard = false;
    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.subjects
    };
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.service.list().subscribe(res => {
      this.subjects = res.data;
    });
  }

  delete(id: any) {
    Swal.fire({
      title: 'Bạn chắc chắn xóa môn học này ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Đồng ý!',
      cancelButtonText: 'Thoát'
    }).then((result) => {
      if (result.value) {
        this.service.delete(id).subscribe(res => {
          Swal.fire({
              icon: 'success',
              title: 'Đã xóa thành công',
              text: '',
              showConfirmButton: false,
              timer: 1000
            }
          );
          this.getAll();
        });
      }
    });
  }

  open(content, id: any) {
    this.service.getSubject(id).subscribe(res => {
      this.subject = res.data;
    });
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
