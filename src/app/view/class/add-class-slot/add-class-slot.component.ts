import {Component, OnInit} from '@angular/core';
import {ClassService} from '../../../service/class.service';
import {Router} from '@angular/router';
import {SlotService} from '../../../service/slot.service';
import {noop} from 'rxjs';
import {Class} from '../../../model/class';
import {Slot} from '../../../model/slot';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-class-slot',
  templateUrl: './add-class-slot.component.html',
  styleUrls: ['./add-class-slot.component.css']
})
export class AddClassSlotComponent implements OnInit {
  isChecked: any;
  clazzs: Class[];
  class: Class = new Class();
  slots: Slot[];
  slot: Slot = new Slot();
  config: any;
  list: string[] = [];
  index: any;

  constructor(private service: ClassService, private slotService: SlotService, private router: Router) { }

  // get accessor
  get value(): any {
    return this.isChecked;
  }

  // set accessor including call the onchange callback
  set value(value: any) {
    this.isChecked = value;
  }

  private onChangeCallback: (_: any) => void = noop;

  ngOnInit() {
    this.service.list().subscribe(res => {
      this.clazzs = res.data;
    });
    this.slotService.list().subscribe(res => {
      this.slots = res.data;
    });
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  onChange(values: any) {
    if (values.currentTarget.checked) {
      this.list.push(values.currentTarget.value);
    } else {
      this.index = this.list.indexOf(values.currentTarget.value);
      this.list.splice(this.index, 1);
    }
  }


  onSubmit() {
    this.service.addClassSlot(this.class, this.list, this.slot.id, this.class.id).subscribe(res => {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1500
      });
      this.router.navigate(['listClass']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }
}
