import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClassSlotComponent } from './add-class-slot.component';

describe('AddClassSlotComponent', () => {
  let component: AddClassSlotComponent;
  let fixture: ComponentFixture<AddClassSlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddClassSlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClassSlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
