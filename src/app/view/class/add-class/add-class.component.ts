import {Component, OnInit} from '@angular/core';
import {Class} from '../../../model/class';
import {ClassService} from '../../../service/class.service';
import {Router} from '@angular/router';
import {Room} from '../../../model/room';
import {RoomService} from '../../../service/room.service';
import Swal from 'sweetalert2';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-class',
  templateUrl: './add-class.component.html',
  styleUrls: ['./add-class.component.css']
})
export class AddClassComponent implements OnInit {
  class: Class = new Class();
  room: Room = new Room();
  rooms: Room[];
  registerForm: FormGroup;
  submitted = false;

  constructor(private service: ClassService, private roomService: RoomService, private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.roomService.list().subscribe(res => {
      this.rooms = res.data;
    });
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      room_id: ['', Validators.required],
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.service.create(this.class, this.class.room_id).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.router.navigate(['listClass']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }
}
