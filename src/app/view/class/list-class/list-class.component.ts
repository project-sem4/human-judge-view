import {Component, OnInit, TemplateRef} from '@angular/core';
import {Class} from '../../../model/class';
import {Router} from '@angular/router';
import {ClassService} from '../../../service/class.service';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {Student} from '../../../model/student';
import {StudentService} from '../../../service/student.service';
import {SmsService} from '../../../service/sms.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Sms} from '../../../model/sms';
import {NoteService} from '../../../service/note.service';
import {Teacher} from '../../../model/teacher';
import {Note} from '../../../model/note';

@Component({
  selector: 'app-list-class',
  templateUrl: './list-class.component.html',
  styleUrls: ['./list-class.component.css']
})
export class ListClassComponent implements OnInit {
  searchText;
  config: any;
  clazzs: Class[];
  clazz: Class = new Class();
  closeResult: string;
  nameSearch: string;
  students: Student[];
  modalRef: BsModalRef;
  sms: Sms = new Sms();
  list: string[] = [];
  private id: any;
  teacher: Teacher = new Teacher();
  note: Note = new Note();
  searchText1;

  // tslint:disable-next-line:max-line-length
  constructor(private service: ClassService, private noteService: NoteService, private studentService: StudentService, private router: Router, config1: NgbModalConfig, private modalService: NgbModal, private smsService: SmsService, private modalService2: BsModalService) {
    config1.backdrop = 'static';
    config1.keyboard = false;
    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.clazzs
    };
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  ngOnInit() {
    this.teacher = JSON.parse(localStorage.getItem('accountDetail'));
    this.getAll();
  }

  getAll() {
    this.service.list().subscribe(res => {
      this.clazzs = res.data;
    });
  }

  edit(id: any) {
    localStorage.setItem('id', id);
    this.router.navigate(['editClass']);
  }

  delete(id: any) {
    Swal.fire({
      title: 'Bạn chắc chắn xóa tài khoản này?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Đồng ý!',
      cancelButtonText: 'Thoát'
    }).then((result) => {
      if (result.value) {
        this.service.delete(id).subscribe(res => {
          Swal.fire({
            icon: 'success',
            title: 'Đã xóa thành công',
            text: '',
            showConfirmButton: false,
            timer: 1000
          });
          this.getAll();
        });
      }
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  search() {
    localStorage.setItem('nameSearch', this.nameSearch);
    this.router.navigate(['searchClass']);
  }

  openClass(danhsachSinhvien, id: any) {
    this.searchText1 = '';
    this.students = [];
    this.studentService.getSudentsByClass(id).subscribe(res => {
      this.students = res.data.listStudent;
    });
    this.modalService.open(danhsachSinhvien, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openModal(id: any, template: TemplateRef<any>) {
    this.modalRef = this.modalService2.show(template);
    this.id = id;
  }

  openModalSMS(parentPhone: any, template: TemplateRef<any>) {
    this.modalRef = this.modalService2.show(template);
    // tslint:disable-next-line:radix
    const newParrentPhone = parseInt(parentPhone);
    this.list.push('+84' + newParrentPhone);
  }

  submit2() {
    this.sms.phone = this.list.toString();
    this.smsService.create(this.sms).subscribe(res => {
      alert('Gửi thành công');
      this.modalRef.hide();
      window.location.reload();
    });
  }

  submit() {
    this.noteService.create(this.id, this.teacher.id, this.note).subscribe(res => {
      this.router.navigate(['note']);
      this.modalRef.hide();
    });
  }
}
