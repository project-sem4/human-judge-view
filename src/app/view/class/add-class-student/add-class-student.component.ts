import {Component, OnInit} from '@angular/core';
import {ClassService} from '../../../service/class.service';
import {Router} from '@angular/router';
import {Class} from '../../../model/class';
import {Student} from '../../../model/student';
import {StudentService} from '../../../service/student.service';
import Swal from 'sweetalert2';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-class-student',
  templateUrl: './add-class-student.component.html',
  styleUrls: ['./add-class-student.component.css']
})
export class AddClassStudentComponent implements OnInit {
  searchText;
  class: Class = new Class();
  config: any;
  clazzs: Class[];
  students: Student[];
  student: Student = new Student();
  isSelected: boolean[];
  list: string[] = [];
  isChecked = false;
  label: string;
  disabled = false;
  className: string;
  index: any;
  registerForm: FormGroup;
  submitted = false;

  // tslint:disable-next-line:max-line-length
  constructor(private service: ClassService, private studentService: StudentService, private router: Router, private formBuilder: FormBuilder) {
  }

  get value(): any {
    return this.isChecked;
  }

  set value(value: any) {
    this.isChecked = value;
  }

  ngOnInit() {
    this.service.list().subscribe(res => {
      this.clazzs = res.data;
    });
    this.studentService.getSudents().subscribe(res => {
      this.students = res.data;
    });
    this.registerForm = this.formBuilder.group({
      room_id: ['', Validators.required],
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  onChange(values: any) {
    if (values.currentTarget.checked) {
      this.list.push(values.currentTarget.value);
    } else {
      this.index = this.list.indexOf(values.currentTarget.value);
      this.list.splice(this.index, 1);
    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.service.addClassStudent(this.class, this.list, this.student.id, this.class.id).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.router.navigate(['listClass']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }
}
