import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClassStudentComponent } from './add-class-student.component';

describe('AddClassStudentComponent', () => {
  let component: AddClassStudentComponent;
  let fixture: ComponentFixture<AddClassStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddClassStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClassStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
