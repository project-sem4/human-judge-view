import {Component, OnInit} from '@angular/core';
import {Class} from '../../../model/class';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {ClassService} from '../../../service/class.service';
import {Room} from '../../../model/room';
import {RoomService} from '../../../service/room.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-class',
  templateUrl: './edit-class.component.html',
  styleUrls: ['./edit-class.component.css']
})
export class EditClassComponent implements OnInit {
  class: Class = new Class();
  id: string;
  rooms: Room[];
  room: Room = new Room();
  registerForm: FormGroup;
  submitted = false;

  // tslint:disable-next-line:max-line-length
  constructor(private http: HttpClient, private roomService: RoomService, private service: ClassService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder) { }

  ngOnInit() {
    const id = localStorage.getItem('id');
    this.service.getClass(id).subscribe(res => {
      this.class = res.data;
    });
    this.roomService.list().subscribe(res => {
      this.rooms = res.data;
    });
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      room_id: ['', Validators.required],
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onEdit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.service.update(this.class.id, this.class.room_id, this.class).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Bạn đã lưu thành công',
          showConfirmButton: false,
          timer: 1000
        });
        this.router.navigate(['listClass']);
      }, error => {
        Swal.fire({
          icon: 'error',
          title: '',
          text: 'Một vài lỗi đã sảy ra!'
        });
      }
    );
  }
}
