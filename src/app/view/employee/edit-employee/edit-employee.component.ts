import {Component, OnInit} from '@angular/core';
import {Employee} from '../../../model/employee';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {EmployeeService} from '../../../service/employee.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {
  employee: Employee = new Employee();
  newVar: Date;
  listOptions;
  id: any;
  registerForm: FormGroup;
  submitted = false;

  // tslint:disable-next-line:max-line-length
  constructor(private http: HttpClient, private service: EmployeeService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.listOptions = [
      {id: 'Male', name: 'Nam'},
      {id: 'Female', name: 'Nữ'},
      {id: 'Others', name: 'Khác'}
    ];
    this.route.queryParams.subscribe(param => {
      this.id = param.id;
    });
    this.service.getEmployee(this.id).subscribe(res => {
      this.employee = res.data;
      this.newVar = new Date(this.employee.dob);
    });
    this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      phone: [this.employee.phone, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      dob: ['', Validators.required],
      idCard: [this.employee.idCard, Validators.compose([Validators.required, Validators.minLength(12), Validators.maxLength(12)])],
      address: ['', Validators.required],
      gender: ['', Validators.required]
    });
  }

  get f() { return this.registerForm.controls; }

  submit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.service.update(this.id, this.employee).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.router.navigate(['listEmployee']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }
}
