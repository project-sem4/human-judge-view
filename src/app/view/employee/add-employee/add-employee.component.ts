import {Component, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import {Employee} from '../../../model/employee';
import {Router} from '@angular/router';
import {EmployeeService} from '../../../service/employee.service';
import Swal from 'sweetalert2';
import {WebCamComponent} from 'ack-angular-webcam';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  employee: Employee = new Employee();
  datePipe = new DatePipe('en-US');
  base64L;
  options = {
    audio: false,
    video: true,
    width: 400,
    height: 400,
  };
  registerForm: FormGroup;
  submitted = false;

  constructor(private service: EmployeeService, private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      phone: [this.employee.phone, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      dob: ['', Validators.required],
      idCard: [this.employee.idCard, Validators.compose([Validators.required, Validators.minLength(12), Validators.maxLength(12)])],
      address: ['', Validators.required],
      gender: ['', Validators.required]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  genBase64(webcam: WebCamComponent) {
    webcam.getBase64()
      .then(base => this.base64L = base)
      .catch(e => console.error(e));
  }

  onCamError(err) {
  }

  onCamSuccess($event: MediaStream) {
  }

  submit() {
    this.submitted = true;
    this.employee.image = this.base64L;
    this.employee.dob = this.datePipe.transform(this.employee.dob, 'yyyy-MM-dd');
    if (this.registerForm.invalid) {
      return;
    }
    this.service.create(this.employee).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.router.navigate(['listEmployee']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }
}
