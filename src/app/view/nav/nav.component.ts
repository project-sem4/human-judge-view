import {Component, OnInit} from '@angular/core';
import {Account} from '../../model/account';
import {Router} from '@angular/router';
import {Credential} from '../../model/credential';
import {Teacher} from '../../model/teacher';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  isCollapsed = false;
  isCollapsed2 = false;
  isCollapsed3 = false;
  isCollapsed4 = false;
  isCollapsed5 = false;
  isCollapsed6 = false;
  isCollapsedT = true;
  isCollapsed7 = false;
  isCollapsed8 = false;
  account1: Account = new Account();
  teacher: Teacher = new Teacher();
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));
  role: string;

  constructor(private router: Router) {
  }

  ngOnInit() {
    const accountCurrent = localStorage.getItem('accountCurrent');
    this.account1 = JSON.parse(accountCurrent);
    this.role = this.account1.role;
    this.teacher = JSON.parse(localStorage.getItem('accountDetail'));
  }

  changeAddAdmin() {
    this.router.navigate(['addAdmin']);
    this.isCollapsed7 = !this.isCollapsed7;
  }

  changeListAdmin() {
    this.router.navigate(['listAdmin']);
    this.isCollapsed7 = !this.isCollapsed7;
  }

  changeAddEmployee() {
    this.router.navigate(['addEmployee']);
    this.isCollapsed8 = !this.isCollapsed8;
  }

  changeListEmployee() {
    this.router.navigate(['listEmployee']);
    this.isCollapsed8 = !this.isCollapsed8;
  }

  changeAddRoom() {
    this.router.navigate(['addRoom']);
    this.isCollapsed5 = !this.isCollapsed5;
  }

  changeListRoom() {
    this.router.navigate(['listRoom']);
    this.isCollapsed5 = !this.isCollapsed5;
  }

  changeAddStudent() {
    this.router.navigate(['addStudent']);
    this.isCollapsed3 = !this.isCollapsed3;
  }

  changeListStudent() {
    this.router.navigate(['listStudent']);
    this.isCollapsed3 = !this.isCollapsed3;
  }

  changeAddClass() {
    this.router.navigate(['addClass']);
    this.isCollapsed2 = !this.isCollapsed2;
  }

  changeListClass() {
    this.router.navigate(['listClass']);
    this.isCollapsed2 = !this.isCollapsed2;
  }

  changeAddTeacher() {
    this.router.navigate(['addTeacher']);
    this.isCollapsed = !this.isCollapsed;
  }

  changeListTeacher() {
    this.router.navigate(['listTeacher']);
    this.isCollapsed = !this.isCollapsed;
  }

  openStudent() {
    this.isCollapsed = false;
    this.isCollapsed2 = false;
    this.isCollapsed3 = !this.isCollapsed3;
    this.isCollapsed4 = false;
    this.isCollapsed5 = false;
    this.isCollapsed6 = false;
    this.isCollapsed7 = false;
    this.isCollapsed8 = false;
  }


  openTeacher() {
    this.isCollapsed = !this.isCollapsed;
    this.isCollapsed2 = false;
    this.isCollapsed3 = false;
    this.isCollapsed4 = false;
    this.isCollapsed5 = false;
    this.isCollapsed6 = false;
    this.isCollapsed7 = false;
    this.isCollapsed8 = false;
  }

  openClass() {
    this.isCollapsed = false;
    this.isCollapsed2 = !this.isCollapsed2;
    this.isCollapsed3 = false;
    this.isCollapsed4 = false;
    this.isCollapsed5 = false;
    this.isCollapsed6 = false;
    this.isCollapsed7 = false;
    this.isCollapsed8 = false;
  }

  changeListSchedule() {
    this.router.navigate(['listSchedule']);
    this.isCollapsed5 = !this.isCollapsed5;
  }

  openLichhoc() {
    this.isCollapsed = false;
    this.isCollapsed2 = false;
    this.isCollapsed3 = false;
    this.isCollapsed4 = false;
    this.isCollapsed5 = !this.isCollapsed5;
    this.isCollapsed6 = false;
    this.isCollapsed7 = false;
    this.isCollapsed8 = false;
  }

  changeAddSchedule() {
    this.router.navigate(['addSchedule']);
    this.isCollapsed5 = !this.isCollapsed5;
  }

  logout() {
    localStorage.removeItem('accountCurrent');
    localStorage.removeItem('id');
    localStorage.removeItem('credentialCurrent');
    localStorage.removeItem('accountDetail');
    window.location.reload();
  }

  openSubject() {
    this.isCollapsed = false;
    this.isCollapsed2 = false;
    this.isCollapsed3 = false;
    this.isCollapsed4 = !this.isCollapsed4;
    this.isCollapsed5 = false;
    this.isCollapsed6 = false;
    this.isCollapsed7 = false;
    this.isCollapsed8 = false;
  }

  changeListSubject() {
    this.router.navigate(['listSubject']);
    this.isCollapsed4 = !this.isCollapsed4;
  }

  changeAddSubject() {
    this.router.navigate(['addSubject']);
    this.isCollapsed4 = !this.isCollapsed4;
  }

  openNews() {
    this.isCollapsed = false;
    this.isCollapsed2 = false;
    this.isCollapsed3 = false;
    this.isCollapsed4 = false;
    this.isCollapsed5 = false;
    this.isCollapsed6 = !this.isCollapsed6;
    this.isCollapsed7 = false;
    this.isCollapsed8 = false;
  }

  changeListNews() {
    this.router.navigate(['listNews']);
    this.isCollapsed6 = !this.isCollapsed6;

  }

  changeAddNews() {
    this.router.navigate(['addNews']);
    this.isCollapsed6 = !this.isCollapsed6;
  }

  changeAddClassStudent() {
    this.router.navigate(['addClassStudent']);
    this.isCollapsed3 = !this.isCollapsed3;

  }

  changeListScheduleStudent() {
    this.router.navigate(['listSchedule']);
    this.isCollapsed5 = !this.isCollapsed5;
  }

  openAdmin() {
    this.isCollapsed = false;
    this.isCollapsed2 = false;
    this.isCollapsed3 = false;
    this.isCollapsed4 = false;
    this.isCollapsed5 = false;
    this.isCollapsed6 = false;
    this.isCollapsed7 = !this.isCollapsed7;
    this.isCollapsed8 = false;
  }

  openEmployee() {
    this.isCollapsed = false;
    this.isCollapsed2 = false;
    this.isCollapsed3 = false;
    this.isCollapsed4 = false;
    this.isCollapsed5 = false;
    this.isCollapsed6 = false;
    this.isCollapsed7 = false;
    this.isCollapsed8 = !this.isCollapsed8;
  }
}
