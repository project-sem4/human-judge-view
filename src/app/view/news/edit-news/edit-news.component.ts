import {Component, OnInit} from '@angular/core';
import {NewsService} from '../../../service/news.service';
import {ActivatedRoute, Router} from '@angular/router';
import {News} from '../../../model/news';
import {Category} from '../../../model/category';
import {Admin} from '../../../model/admin';
import Swal from 'sweetalert2';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CategoryService} from '../../../service/category.service';

@Component({
  selector: 'app-edit-news',
  templateUrl: './edit-news.component.html',
  styleUrls: ['./edit-news.component.css']
})
export class EditNewsComponent implements OnInit {
  new: News = new News();
  category: Category = new Category();
  admin: Admin;
  categorys: Category[];
  id: any;
  registerForm: FormGroup;
  submitted = false;

  constructor(private service: NewsService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, private categoryService: CategoryService) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(param => {
      this.id = param.id;
    });
    this.admin = JSON.parse(localStorage.getItem('accountDetail'));
    this.service.getNew(this.id).subscribe(res => {
      this.new = res.data;
    });
    this.categoryService.list().subscribe(res => {
      this.categorys = res.data;
    });
    this.registerForm = this.formBuilder.group({
      categoryId: ['', Validators.required],
      title: [this.new.title, Validators.compose([Validators.required,  Validators.minLength(4)])],
      description: [this.new.description, Validators.compose([Validators.required,  Validators.minLength(8)])],
      content: [this.new.content, Validators.compose([Validators.required,  Validators.minLength(16)])],
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onEdit() {
    this.submitted = true;
    this.new.adminId = this.admin.id;
    if (this.registerForm.invalid) {
      return;
    }
    this.service.update(this.id, this.new).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.router.navigate(['listNews']);
      }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
      }
    );
  }

}
