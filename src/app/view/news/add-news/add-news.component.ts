import {Component, OnInit} from '@angular/core';
import {NewsService} from '../../../service/news.service';
import {Router} from '@angular/router';
import {News} from '../../../model/news';
import {Category} from '../../../model/category';
import {Admin} from '../../../model/admin';
import {CategoryService} from '../../../service/category.service';
import Swal from 'sweetalert2';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.css']
})
export class AddNewsComponent implements OnInit {
  new: News = new News();
  category: Category = new Category();
  categorys: Category[];
  admin: Admin;
  registerForm: FormGroup;
  submitted = false;

  // tslint:disable-next-line:max-line-length
  constructor(private service: NewsService, private categoryService: CategoryService, private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.new.content = '';
    this.admin = JSON.parse(localStorage.getItem('accountDetail'));
    // tslint:disable-next-line:label-position
    this.categoryService.list().subscribe(res => {
      this.categorys = res.data;
    });
    this.registerForm = this.formBuilder.group({
      categoryId: ['', Validators.required],
      title: [this.new.title, Validators.compose([Validators.required,  Validators.minLength(4)])],
      description: [this.new.description, Validators.compose([Validators.required,  Validators.minLength(8)])],
      content: [this.new.content, Validators.compose([Validators.required,  Validators.minLength(16)])],
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.new.adminId = this.admin.id;
    if (this.registerForm.invalid) {
      return;
    }
    this.service.create(this.new).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.router.navigate(['listNews']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }
}
