import {Component, OnInit} from '@angular/core';
import {Room} from '../../../model/room';
import {RoomService} from '../../../service/room.service';
import {Router} from '@angular/router';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {Class} from '../../../model/class';

@Component({
  selector: 'app-list-room',
  templateUrl: './list-room.component.html',
  styleUrls: ['./list-room.component.css']
})
export class ListRoomComponent implements OnInit {
  searchText;
  config: any;
  rooms: Room[];
  room: Room = new Room();
  class: Class = new Class();

  constructor(private service: RoomService, private router: Router, config1: NgbModalConfig, private modalService: NgbModal) {
    config1.backdrop = 'static';
    config1.keyboard = false;
    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.rooms
    };
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.service.list().subscribe(res => {
      this.rooms = res.data;
    });
  }
}
