import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TeacherService} from '../../../service/teacher.service';
import {Teacher} from '../../../model/teacher';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-teacher',
  templateUrl: './list-teacher.component.html',
  styleUrls: ['./list-teacher.component.css']
})
export class ListTeacherComponent implements OnInit {

  searchText;
  list: string[] = [];
  config: any;
  teachers: Teacher[];
  teacher: Teacher = new Teacher();
  closeResult: string;

  constructor(private service: TeacherService, private router: Router, config1: NgbModalConfig, private modalService: NgbModal) {
    config1.backdrop = 'static';
    config1.keyboard = false;
    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.teachers
    };
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.service.list().subscribe(res => {
      this.teachers = res.data;
    });
  }

  delete(id: any) {
    Swal.fire({
      title: 'Bạn chắc chắn xóa tài khoản này ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Đồng ý!',
      cancelButtonText: 'Thoát'
    }).then((result) => {
      if (result.value) {
        this.service.delete(id).subscribe(res => {
          Swal.fire({
            icon: 'success',
            title: 'Đã xóa thành công',
            text: '',
            showConfirmButton: false,
            timer: 1000
          });
          this.getAll();
        });
      }
    });
  }

  open(content, id: string) {
    this.service.getTeacher(id).subscribe(res => {
      this.teacher = res.data;
    });
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  private displayGender(gender) {
    if (gender === 'Male') {
      return 'Nam';
    } else if (gender === 'Female') {
      return 'Nữ';
    } else if (gender === 'Others') {
      return 'Khác';
    }
  }
}


