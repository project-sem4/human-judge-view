import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Teacher} from '../../../model/teacher';
import {TeacherService} from '../../../service/teacher.service';
import {DatePipe} from '@angular/common';
import Swal from 'sweetalert2';
import {WebCamComponent} from 'ack-angular-webcam';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-teacher',
  templateUrl: './add-teacher.component.html',
  styleUrls: ['./add-teacher.component.css']
})
export class AddTeacherComponent implements OnInit {
  teacher: Teacher = new Teacher();
  datePipe = new DatePipe('en-US');
  newVar: Date;
  base64L;
  options = {
    audio: false,
    video: true,
    width: 400,
    height: 400,
  };
  registerForm: FormGroup;
  submitted = false;

  constructor(private service: TeacherService, private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      phone: [this.teacher.phone, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      dob: ['', Validators.required],
      idCard: [this.teacher.idCard, Validators.compose([Validators.required, Validators.minLength(12), Validators.maxLength(12)])],
      address: ['', Validators.required],
      gender: ['', Validators.required]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  genBase64(webcam: WebCamComponent) {
    webcam.getBase64()
      .then(base => this.base64L = base)
      .catch(e => console.error(e));
  }

  submit() {
    this.submitted = true;
    this.teacher.image = this.base64L;
    this.teacher.dob = this.datePipe.transform(this.teacher.dob, 'yyyy-MM-dd');
    if (this.registerForm.invalid) {
      return;
    }
    this.service.create(this.teacher).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.router.navigate(['listTeacher']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }
}
