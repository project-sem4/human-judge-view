import {Component, OnInit} from '@angular/core';
import Swal from 'sweetalert2';
import {DatePipe} from '@angular/common';
import {Admin} from '../../../model/admin';
import {Router} from '@angular/router';
import {AdminService} from '../../../service/admin.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-admin',
  templateUrl: './add-admin.component.html',
  styleUrls: ['./add-admin.component.css']
})
export class AddAdminComponent implements OnInit {
  admin: Admin = new Admin();
  datePipe = new DatePipe('en-US');
  newVar: any;
  registerForm: FormGroup;
  submitted = false;

  constructor(private service: AdminService, private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      phone: [this.admin.phone, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      dob: ['', Validators.required],
      address: ['', Validators.required]
    });
  }

  get f() { return this.registerForm.controls; }

  submit() {
    this.submitted = true;
    this.admin.dob = this.datePipe.transform(this.admin.dob, 'yyyy-MM-dd');
    if (this.registerForm.invalid) {
      return;
    }
    this.service.create(this.admin).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.router.navigate(['listAdmin']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }
}
