import {Component, OnInit} from '@angular/core';
import {Student} from '../../../model/student';
import {StudentService} from '../../../service/student.service';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import Swal from 'sweetalert2';
import {WebCamComponent} from 'ack-angular-webcam';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {
  student: Student = new Student();
  datePipe = new DatePipe('en-US');
  base64L;
  options = {
    audio: false,
    video: true,
    width: 400,
    height: 400,
  };
  registerForm: FormGroup;
  submitted = false;

  constructor(private service: StudentService, private route: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      phone: [this.student.phone, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      dob: ['', Validators.required],
      startYear: ['', Validators.required],
      endYear: ['', Validators.required],
      idCard: [this.student.idCard, Validators.compose([Validators.required, Validators.minLength(12), Validators.maxLength(12)])],
      address: ['', Validators.required],
      gender: ['', Validators.required],
      // tslint:disable-next-line:max-line-length
      parentPhone: [this.student.parentPhone, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  genBase64(webcam: WebCamComponent) {
    webcam.getBase64()
      .then(base => this.base64L = base)
      .catch(e => console.error(e));
  }

  onCamError(err) {
  }

  onCamSuccess($event: MediaStream) {
  }

  submit() {
    this.submitted = true;
    this.student.image = this.base64L;
    this.student.dob = this.datePipe.transform(this.student.dob, 'yyyy-MM-dd');
    this.student.startYear = this.datePipe.transform(this.student.startYear, 'yyyy-MM-dd');
    this.student.endYear = this.datePipe.transform(this.student.endYear, 'yyyy-MM-dd');
    if (this.registerForm.invalid) {
      return;
    }
    this.service.create(this.student).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.route.navigate(['listStudent']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }
}
