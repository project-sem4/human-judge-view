import {Component, OnInit} from '@angular/core';
import {Student} from '../../../model/student';
import {HttpClient} from '@angular/common/http';
import {StudentService} from '../../../service/student.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import Swal from 'sweetalert2';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css']
})
export class EditStudentComponent implements OnInit {

  student: Student = new Student();
  listOptions;
  newVar: Date;
  // tslint:disable-next-line:variable-name
  start_year: Date;
  // tslint:disable-next-line:variable-name
  end_year: Date;
  datePipe = new DatePipe('en-US');
  id: any;
  registerForm: FormGroup;
  submitted = false;

  // tslint:disable-next-line:max-line-length
  constructor(private http: HttpClient, private service: StudentService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.listOptions = [
      {id: 'Male', name: 'Nam'},
      {id: 'Female', name: 'Nữ'},
      {id: 'Others', name: 'Khác'}
    ];
    this.route.queryParams.subscribe(param => {
      this.id = param.id;
    });
    this.service.getStudent(this.id).subscribe(res => {
      this.student = res.data;
      this.newVar = new Date(this.student.dob);
      this.start_year = new Date(this.student.startYear);
      this.end_year = new Date(this.student.endYear);
    });
    this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      phone: [this.student.phone, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      dob: ['', Validators.required],
      startYear: ['', Validators.required],
      endYear: ['', Validators.required],
      idCard: [this.student.idCard, Validators.compose([Validators.required, Validators.minLength(12), Validators.maxLength(12)])],
      address: ['', Validators.required],
      gender: ['', Validators.required],
      // tslint:disable-next-line:max-line-length
      parentPhone: [this.student.parentPhone, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  submit() {
    this.submitted = true;
    this.student.dob = this.datePipe.transform(this.student.dob, 'yyyy-MM-dd');
    this.student.startYear = this.datePipe.transform(this.student.startYear, 'yyyy-MM-dd');
    this.student.endYear = this.datePipe.transform(this.student.endYear, 'yyyy-MM-dd');
    if (this.registerForm.invalid) {
      return;
    }
    this.service.update(this.id, this.student).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.router.navigate(['listStudent']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }
}
