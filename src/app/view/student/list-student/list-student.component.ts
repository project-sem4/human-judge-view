import {Component, Input, NgZone, OnInit, TemplateRef} from '@angular/core';
import {Student} from '../../../model/student';
import {StudentService} from '../../../service/student.service';
import {Router} from '@angular/router';
import {ModalDismissReasons, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Note} from '../../../model/note';
import {NoteService} from '../../../service/note.service';
import {SmsService} from '../../../service/sms.service';
import {Sms} from '../../../model/sms';
import {FileUploader, FileUploaderOptions, ParsedResponseHeaders} from 'ng2-file-upload';
import {Teacher} from '../../../model/teacher';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-student',
  templateUrl: './list-student.component.html',
  styleUrls: ['./list-student.component.css']
})
export class ListStudentComponent implements OnInit {
  @Input()
  responses: Array<any>;

  private cloudinaryName = 'dvlsudref';
  private cloudinaryPreset = 'qr43fi3a';
  private hasBaseDropZoneOver = false;
  private uploader: FileUploader;
  private title: string;
  sms: Sms = new Sms();
  searchText;
  list: string[] = [];
  config: any;
  students: Student[];
  student: Student = new Student();
  closeResult: string;
  modalRef: BsModalRef;
  id: any;
  note: Note = new Note();
  teacher: Teacher = new Teacher();
  image: any;
  sms1: any;

  // tslint:disable-next-line:max-line-length
  constructor(private zone: NgZone, private service: StudentService, private noteService: NoteService, private smsService: SmsService, private router: Router, config1: NgbModalConfig, private modalService: NgbModal, private modalService2: BsModalService) {
    config1.backdrop = 'static';
    config1.keyboard = false;
    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.students
    };
    this.responses = [];
    this.title = '';
  }

  ngOnInit() {
    this.teacher = JSON.parse(localStorage.getItem('accountDetail'));
    // Create the file uploader, wire it to upload to your account
    const uploaderOptions: FileUploaderOptions = {
      url: `https://api.cloudinary.com/v1_1/${this.cloudinaryName}/upload`,
      // Upload files automatically upon addition to upload queue
      autoUpload: true,
      // Use xhrTransport in favor of iframeTransport
      isHTML5: true,
      // Calculate progress independently for each uploaded file
      removeAfterUpload: true,
      // XHR request headers
      headers: [
        {
          name: 'X-Requested-With',
          value: 'XMLHttpRequest'
        }
      ]
    };
    this.uploader = new FileUploader(uploaderOptions);

    this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
      // Add Cloudinary's unsigned upload preset to the upload form
      form.append('upload_preset', this.cloudinaryPreset);
      // Add built-in and custom tags for displaying the uploaded photo in the list
      let tags = 'myphotoalbum';
      if (this.title) {
        form.append('context', `photo=${this.title}`);
        tags = `myphotoalbum,${this.title}`;
      }
      // Upload to a custom folder
      // Note that by default, when uploading via the API, folders are not automatically created in your Media Library.
      // In order to automatically create the folders based on the API requests,
      // please go to your account upload settings and set the 'Auto-create folders' option to enabled.
      form.append('folder', 'angular_sample');
      // Add custom tags
      form.append('tags', tags);
      // Add file to upload
      form.append('file', fileItem);

      // Use default "withCredentials" value for CORS requests
      fileItem.withCredentials = false;
      return {fileItem, form};
    };

    // Insert or update an entry in the responses array
    const upsertResponse = fileItem => {

      // Run the update in a custom zone since for some reason change detection isn't performed
      // as part of the XHR request to upload the files.
      // Running in a custom zone forces change detection
      this.zone.run(() => {
        // Update an existing entry if it's upload hasn't completed yet

        // Find the id of an existing item
        const existingId = this.responses.reduce((prev, current, index) => {
          if (current.file.name === fileItem.file.name && !current.status) {
            return index;
          }
          return prev;
        }, -1);
        if (existingId > -1) {
          // Update existing item with new data
          this.responses[existingId] = Object.assign(this.responses[existingId], fileItem);
        } else {
          // Create new response
          this.responses.push(fileItem);
        }
      });
    };

    // Update model on completion of uploading a file
    this.uploader.onCompleteItem = (item: any, response: string, status: number, headers: ParsedResponseHeaders) =>
      upsertResponse(
        {
          file: item.file,
          status,
          data: JSON.parse(response)
        }
      );

    // Update model on upload progress event
    this.uploader.onProgressItem = (fileItem: any, progress: any) =>
      upsertResponse(
        {
          file: fileItem.file,
          progress,
          data: {}
        }
      );
    this.getAll();
  }

  // Delete an uploaded image
  // Requires setting "Return delete token" to "Yes" in your upload preset configuration
  // See also https://support.cloudinary.com/hc/en-us/articles/202521132-How-to-delete-an-image-from-the-client-side-
  deleteImage = function(data: any, index: number) {
    const url = `https://api.cloudinary.com/v1_1/${this.cloudinary.config().cloud_name}/delete_by_token`;
    const headers = new Headers({'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest'});
    const options = {headers};
    const body = {
      token: data.delete_token
    };
    this.http.post(url, body, options).subscribe(response => {
      // Remove deleted item for responses
      this.responses.splice(index, 1);
    });
  };

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  private displayGender(gender) {
    if (gender === 'Male') {
      return 'Nam';
    } else if (gender === 'Female') {
      return 'Nữ';
    } else if (gender === 'Others') {
      return 'Khác';
    }
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  openModal(id: any, template: TemplateRef<any>) {
    this.modalRef = this.modalService2.show(template);
    this.id = id;
  }

  openModalSMS(parentPhone: any, template: TemplateRef<any>) {
    this.modalRef = this.modalService2.show(template);
    // tslint:disable-next-line:radix
    const newParrentPhone = parseInt(parentPhone);
    this.list.push('+84' + newParrentPhone);
  }

  openModalUpdate(id: any, image: any, template: TemplateRef<any>) {
    this.modalRef = this.modalService2.show(template);
    this.id = id;
    this.image = image;
  }

  getAll() {
    this.service.getSudents().subscribe(res => {
      this.students = res.data;
    });
  }

  delete(id: any) {
    Swal.fire({
      title: 'Bạn chắc chắn xóa sinh viên này?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Đồng ý!',
      cancelButtonText: 'Thoát'
    }).then((result) => {
      if (result.value) {
        this.service.delete(id).subscribe(res => {
          Swal.fire({
            icon: 'success',
            title: 'Đã xóa thành công',
            text: '',
            showConfirmButton: false,
            timer: 1000
          });
          this.getAll();
        });
      }
    });
  }

  open(content, id: string) {
    this.service.getStudent(id).subscribe(res => {
      this.student = res.data;
      this.student.image = res.data.image.split(',')[0];
    });
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  submit3() {
    // tslint:disable-next-line:prefer-const
    let str = this.image.toString();
    const arr = str.split(',');
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < arr.length; i++) {
      this.list.push(arr[i]);
    }
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.responses.length; i++) {
      this.list.push(this.responses[i].data.url);
    }
    this.student.image = this.list.toString();
    this.service.updateImage(this.id, this.student).subscribe(res => {
      alert('Gửi thành công');
      this.modalRef.hide();
      window.location.reload();
    });
  }

  submit2() {
    this.sms.phone = this.list.toString();
    this.smsService.create(this.sms).subscribe(res => {
      alert('Gửi thành công');
      this.modalRef.hide();
      window.location.reload();
    });
  }

  submit() {
    this.noteService.create(this.id, this.teacher.id, this.note).subscribe(res => {
      this.router.navigate(['note']);
      this.modalRef.hide();
    });
  }
}
