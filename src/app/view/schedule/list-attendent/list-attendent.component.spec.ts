import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAttendentComponent } from './list-attendent.component';

describe('ListAttendentComponent', () => {
  let component: ListAttendentComponent;
  let fixture: ComponentFixture<ListAttendentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAttendentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAttendentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
