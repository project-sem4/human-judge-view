import { Component, OnInit } from '@angular/core';
import {Note} from '../../../model/note';
import {BsModalService} from 'ngx-bootstrap';
import {Router} from '@angular/router';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {AttendentService} from '../../../service/attendent.service';
import {Attendent} from '../../../model/attendent';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-list-attendent',
  templateUrl: './list-attendent.component.html',
  styleUrls: ['./list-attendent.component.css']
})
export class ListAttendentComponent implements OnInit {
  searchText;
  config: any;
  attendent: Attendent;
  attendents: Attendent[];
  note: Note = new Note();
  id: any;

  // tslint:disable-next-line:max-line-length
  constructor(private service: AttendentService, private router: Router, config1: NgbModalConfig, private modalService: NgbModal, private modalService2: BsModalService) {
    config1.backdrop = 'static';
    config1.keyboard = false;
    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.attendents
    };
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  ngOnInit() {
    const id = localStorage.getItem('ScheduleId');
    this.service.getAttendentByScheduleId(id).subscribe(res => {
      this.attendents = res.data.attendanceDTOS;
    });

  }

  onChange(values: any, id: any) {
    if (values.currentTarget.checked) {
      this.attendent = new Attendent();
      this.attendent.status = 'Attended';
      this.service.updateAttendent(this.attendent, id).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Bạn đã điểm  thành công',
          showConfirmButton: false,
          timer: 1000
        });
      });
    } else {
      this.attendent = new Attendent();
      this.attendent.status = 'Absent';
      this.service.updateAttendent(this.attendent, id).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Bạn đã sửa thành công',
          showConfirmButton: false,
          timer: 1000
        });

      });
    }
  }
}
