import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import 'dhtmlx-scheduler';
import {EventService} from '../../../service/event.service';
import {Router} from '@angular/router';
import {Schedule} from '../../../model/schedule';
import {Event} from '../../../model/event';
import {ScheduleService} from '../../../service/schedule.service';
import {Attendent} from '../../../model/attendent';
import {AttendentService} from '../../../service/attendent.service';
import Swal from 'sweetalert2';
import {Class} from '../../../model/class';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'scheduler',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './list-schedule.component.html',
  styleUrls: ['./list-schedule.component.css']
})
export class ListScheduleComponent implements OnInit {
  events: Event[] = [];
  class: Class;
  classId: any;
  schedule: Schedule = new Schedule();
  schedule1: Schedule = new Schedule();
  currentAttendance: Attendent;
  list = [];
  // @ts-ignore
  @ViewChild('scheduler_here') schedulerContainer: ElementRef;
  searchText: any;

  // tslint:disable-next-line:max-line-length
  constructor(private eventService: EventService, private router: Router, private scheduleSevice: ScheduleService, private attendentService: AttendentService) {
  }

  ngOnInit() {
    this.eventService.get()
      .then((data) => {
        this.events = data.data;
        localStorage.setItem('events', JSON.stringify(this.events));
        scheduler.parse(data, 'json');
      });
    // tslint:disable-next-line:only-arrow-functions
    const html = function(id) {
      return document.getElementById(id);
    }; // just a helper
    scheduler.config.xml_date = '%Y-%m-%d %H:%i';
    scheduler.init(this.schedulerContainer.nativeElement, new Date());
    scheduler.attachEvent('onLightboxButton', (id, ev) => {
      scheduler.showLightbox(id);
    });

    function saveScheduleToLocalStorage(id: string) {
      const existAttendance = JSON.parse(localStorage.getItem('currentAttendance'));
      if (existAttendance != null) {
        localStorage.removeItem('currentAttendance');
      }
      const credential = JSON.parse(localStorage.getItem('credentialCurrent'));
      const student = JSON.parse(localStorage.getItem('accountDetail'));
      const BASE_URL = 'https://spring-boot-university.herokuapp.com';
      const API_PATH = '/_api/v1/attendances/getAttendanceByStudentAndSchedule';
      const PARAMS = '?studentId=' + student.id + '&scheduleId=' + id;
      const xhr = new XMLHttpRequest();
      xhr.onload = () => {
        if (xhr.status === 200) {
          const object = JSON.parse(xhr.response);
          localStorage.setItem('currentAttendance', JSON.stringify(object.data));
        }
      };
      xhr.open('GET', BASE_URL + API_PATH + PARAMS, false);
      xhr.setRequestHeader('Authorization', 'Bearer ' + credential.accessToken);
      xhr.send();
    }

    // tslint:disable-next-line:only-arrow-functions
    // @ts-ignore
    // tslint:disable-next-line:only-arrow-functions
    scheduler.showLightbox = async function(id) {
      // tslint:disable-next-line:label-position no-unused-expression
      const eventCurrent = JSON.parse(localStorage.getItem('events'));
      // tslint:disable-next-line:prefer-const
      const accountCurrent = JSON.parse(localStorage.getItem('accountCurrent'));
      const role = accountCurrent.role;
      let status;
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < eventCurrent.length; i++) {
        if (eventCurrent[i].id === id) {
          status = eventCurrent[i].status;
          if (role === 'Student') {
            saveScheduleToLocalStorage(id);
            await new Promise(resolve => setTimeout(resolve, 1000));
          }
        }
      }

      if (role === 'Teacher' || role === 'Manager' || role === 'Admin') {
        if (status === 'Pending') {
          scheduler.startLightbox(id, html('my_form'));
          html('description').innerText = 'Lớp học đang chờ';
          (html('descriptionInp') as HTMLInputElement).value = id;
        } else if (status === 'Done') {
          scheduler.startLightbox(id, html('my_form1'));
          html('description1').innerText = 'Điểm danh thành công, vui lòng kiểm tra';
          (html('descriptionInp1') as HTMLInputElement).value = id;
        } else {
          scheduler.startLightbox(id, html('my_form2'));
          html('description2').innerText = 'Lớp học mới được bắt đầu';
          (html('descriptionInp2') as HTMLInputElement).value = id;
        }
      } else if (role === 'Student') {
        const currentAttendance = JSON.parse(localStorage.getItem('currentAttendance'));
        scheduler.startLightbox(id, html('my_form3'));
        if (currentAttendance != null) {
          if (currentAttendance.status === 'Attended') {
            html('description3').innerText = 'Bạn đã đươc điểm danh';
            (html('descriptionInp3') as HTMLInputElement).value = id;
          } else if (currentAttendance.status === 'Absent') {
            html('description3').innerText = 'Bạn chưa điểm danh';
            (html('descriptionInp3') as HTMLInputElement).value = id;
          }
        } else {
          html('description3').innerText = 'Lớp học đang chờ';
          (html('descriptionInp3') as HTMLInputElement).value = id;
        }
      }
    };
  }

  close_form() {
    localStorage.removeItem('currentAttendance');
    scheduler.endLightbox(false, document.getElementById('my_form'));
  }

  getScheduleById(data: any, s: any): any {
    return data.filter(e => e.id.includes(s))
      .sort((a) => a.id.includes(s) && !a.id.includes(s) ? 1 : 0);
  }

  active() {
    // tslint:disable-next-line:prefer-const
    let scheduleId = (document.getElementById('descriptionInp') as HTMLInputElement).value;
    this.schedule1 = this.getScheduleById(this.events, scheduleId);
    this.classId = this.schedule1[0].classId;
    this.scheduleSevice.activeSchedule(scheduleId).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã kích hoạt chế độ điểm danh tự động',
        showConfirmButton: false,
        timer: 1000
      });
      scheduler.endLightbox(false, document.getElementById('my_form'));
      window.location.reload();
    });
  }

  attendent() {
    scheduler.endLightbox(false, document.getElementById('my_form1'));
    // tslint:disable-next-line:prefer-const
    let scheduleId = (document.getElementById('descriptionInp1') as HTMLInputElement).value;
    localStorage.setItem('ScheduleId', scheduleId);
    this.router.navigate(['listAttendent']);
  }

  close_form1() {
    scheduler.endLightbox(false, document.getElementById('my_form1'));
  }

  close_form2() {
    scheduler.endLightbox(false, document.getElementById('my_form2'));
  }

  close_form3() {
    scheduler.endLightbox(false, document.getElementById('my_form3'));
  }
}
