import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ClassService} from '../../../service/class.service';
import {DayTypeService} from '../../../service/day-type.service';
import {SubjectService} from '../../../service/subject.service';
import {RoomService} from '../../../service/room.service';
import {SlotService} from '../../../service/slot.service';
import {Employee} from '../../../model/employee';
import {EmployeeService} from '../../../service/employee.service';
import {TeacherService} from '../../../service/teacher.service';
import {Teacher} from '../../../model/teacher';
import {Class} from '../../../model/class';
import {DayType} from '../../../model/day-type';
import {Subject} from '../../../model/subject';
import {Room} from '../../../model/room';
import {Slot} from '../../../model/slot';
import {Schedule} from '../../../model/schedule';
import {ScheduleService} from '../../../service/schedule.service';
import {noop} from 'rxjs';
import {DatePipe} from '@angular/common';
import Swal from 'sweetalert2';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-schedule',
  templateUrl: './add-schedule.component.html',
  styleUrls: ['./add-schedule.component.css']
})
export class AddScheduleComponent implements OnInit {
  searchText;
  employee3: Employee = JSON.parse(localStorage.getItem('accountDetail'));
  schedules: Schedule[];
  schedule: Schedule = new Schedule();
  teachers: Teacher[];
  teacher: Teacher = new Teacher();
  clazzs: Class[];
  clazz: Class = new Class();
  dayTypes: DayType[];
  dayType: DayType = new DayType();
  subjects: Subject[];
  subject: Subject = new Subject();
  rooms: Room[];
  room: Room = new Room();
  slots: Slot[];
  slot: Slot = new Slot();
  employees: Employee[];
  employee: Employee = new Employee();
  isChecked: any;
  config: any;
  list: string[] = [];
  index: any;
  datePipe = new DatePipe('en-US');
  registerForm: FormGroup;
  submitted = false;

  constructor(private router: Router, private service: ScheduleService, private techerService: TeacherService,
              private classService: ClassService, private dayTypeService: DayTypeService, private subjectService: SubjectService,
              private roomService: RoomService, private employeeService: EmployeeService, private slotService: SlotService,
              private formBuilder: FormBuilder) {
  }

  // get accessor
  get value(): any {
    return this.isChecked;
  }

  // set accessor including call the onchange callback
  set value(value: any) {
    this.isChecked = value;
  }

  private onChangeCallback: (_: any) => void = noop;

  ngOnInit() {
    this.techerService.list().subscribe(res => {
      this.teachers = res.data;
    });
    this.classService.list().subscribe(res => {
      this.clazzs = res.data;
    });
    this.dayTypeService.list().subscribe(res => {
      this.dayTypes = res.data;
    });
    this.subjectService.list().subscribe(res => {
      this.subjects = res.data;
    });
    this.roomService.list().subscribe(res => {
      this.rooms = res.data;
    });
    this.employeeService.list().subscribe(res => {
      this.employees = res.data;
    });
    this.slotService.list().subscribe(res => {
      this.slots = res.data;
    });
    this.registerForm = this.formBuilder.group({
      clazzId: ['', Validators.required],
      teacherId: ['', Validators.required],
      dayTypeId: ['', Validators.required],
      subjectId: ['', Validators.required],
      roomId: ['', Validators.required],
      startTime: ['', Validators.required],
      endTime: ['', Validators.required],
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  onChange(values: any) {
    if (values.currentTarget.checked) {
      this.list.push(values.currentTarget.value);
    } else {
      this.index = this.list.indexOf(values.currentTarget.value);
      this.list.splice(this.index, 1);
    }
  }

  onSubmit() {
    this.submitted = true;
    this.schedule.startTime = this.datePipe.transform(this.schedule.startTime, 'yyyy-MM-dd');
    this.schedule.endTime = this.datePipe.transform(this.schedule.endTime, 'yyyy-MM-dd');
    this.schedule.slotId = this.list.toString();
    this.schedule.employeeId = this.employee3.id;
    if (this.registerForm.invalid) {
      return;
    }
    this.service.create(this.schedule).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Bạn đã lưu thành công',
        showConfirmButton: false,
        timer: 1000
      });
      this.router.navigate(['listSchedule']);
    }, error => {
      Swal.fire({
        icon: 'error',
        title: '',
        text: 'Một vài lỗi đã sảy ra!'
      });
    });
  }


}
