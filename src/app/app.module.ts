import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ListTeacherComponent} from './view/teacher/list-teacher/list-teacher.component';
import {AddTeacherComponent} from './view/teacher/add-teacher/add-teacher.component';
import {AddStudentComponent} from './view/student/add-student/add-student.component';
import {ListStudentComponent} from './view/student/list-student/list-student.component';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AddClassComponent} from './view/class/add-class/add-class.component';
import {ListClassComponent} from './view/class/list-class/list-class.component';
import {ListRoomComponent} from './view/room/list-room/list-room.component';
import {ListNoteComponent} from './view/note/list-note/list-note.component';
import {AddSubjectComponent} from './view/subject/add-subject/add-subject.component';
import {ListSubjectComponent} from './view/subject/list-subject/list-subject.component';
import {LoginComponent} from './account/login/login.component';
import {StudentService} from './service/student.service';
import {TeacherService} from './service/teacher.service';
import {RoomService} from './service/room.service';
import {ClassService} from './service/class.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EditClassComponent} from './view/class/edit-class/edit-class.component';
import {EditStudentComponent} from './view/student/edit-student/edit-student.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination';
import {EditTeacherComponent} from './view/teacher/edit-teacher/edit-teacher.component';
import {DashboardComponent} from './view/dashboard/dashboard.component';
import {MatDatepickerModule} from '@angular/material';
import {ListNewsComponent} from './view/news/list-news/list-news.component';
import {AddNewsComponent} from './view/news/add-news/add-news.component';
import {EditNewsComponent} from './view/news/edit-news/edit-news.component';
import {AddClassStudentComponent} from './view/class/add-class-student/add-class-student.component';
import {AddClassSlotComponent} from './view/class/add-class-slot/add-class-slot.component';
import {BsModalService, CollapseModule} from 'ngx-bootstrap';
import {ModalModule} from 'ngx-bootstrap/modal';
import {EditSubjectComponent} from './view/subject/edit-subject/edit-subject.component';
import {AddScheduleComponent} from './view/schedule/add-schedule/add-schedule.component';
// search module
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import {NavComponent} from './view/nav/nav.component';
import {EditScheduleComponent} from './view/schedule/edit-schedule/edit-schedule.component';
import {FileUploadModule} from 'ng2-file-upload';
import {ListScheduleComponent} from './view/schedule/list-schedule/list-schedule.component';
import {ListAttendentComponent} from './view/schedule/list-attendent/list-attendent.component';
import {IndexComponent} from './view/homeUniversity/index/index.component';
import {AboutComponent} from './view/homeUniversity/about/about.component';
import {NewsComponent} from './view/homeUniversity/news/news.component';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {Nav2Component} from './view/homeUniversity/nav2/nav2.component';
import {ContactComponent} from './view/homeUniversity/contact/contact.component';
import {FooterComponent} from './view/homeUniversity/footer/footer.component';
import {AddEmployeeComponent} from './view/employee/add-employee/add-employee.component';
import {EditEmployeeComponent} from './view/employee/edit-employee/edit-employee.component';
import {ListEmployeeComponent} from './view/employee/list-employee/list-employee.component';
import {AddAdminComponent} from './view/admin/add-admin/add-admin.component';
import {ListAdminComponent} from './view/admin/list-admin/list-admin.component';
import {EditAdminComponent} from './view/admin/edit-admin/edit-admin.component';
import {WebCamModule} from 'ack-angular-webcam';
import {EditorModule} from '@tinymce/tinymce-angular';

@NgModule({
  declarations: [
    AppComponent,
    ListTeacherComponent,
    AddTeacherComponent,
    AddStudentComponent,
    ListStudentComponent,
    AddClassComponent,
    ListClassComponent,
    ListRoomComponent,
    ListNoteComponent,
    AddSubjectComponent,
    ListSubjectComponent,
    LoginComponent,
    EditClassComponent,
    EditStudentComponent,
    EditTeacherComponent,
    DashboardComponent,
    ListNewsComponent,
    AddNewsComponent,
    EditNewsComponent,
    AddClassStudentComponent,
    AddClassSlotComponent,
    EditSubjectComponent,
    AddScheduleComponent,
    NavComponent,
    EditScheduleComponent,
    ListScheduleComponent,
    ListAttendentComponent,
    IndexComponent,
    AboutComponent,
    NewsComponent,
    Nav2Component,
    ContactComponent,
    FooterComponent,
    AddEmployeeComponent,
    EditEmployeeComponent,
    ListEmployeeComponent,
    AddAdminComponent,
    ListAdminComponent,
    EditAdminComponent
  ],
  imports: [
    CKEditorModule,
    Ng2SearchPipeModule,
    BrowserModule,
    AppRoutingModule,
    NgxPaginationModule,
    NgbModule,
    BsDatepickerModule.forRoot(),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MatDatepickerModule,
    WebCamModule,
    ModalModule.forRoot(),
    CollapseModule,
    FileUploadModule,
    EditorModule,
    ReactiveFormsModule
  ],
  providers: [
    BsModalService,
    StudentService,
    TeacherService,
    RoomService,
    ClassService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
