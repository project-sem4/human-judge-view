import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListTeacherComponent} from './view/teacher/list-teacher/list-teacher.component';
import {AddTeacherComponent} from './view/teacher/add-teacher/add-teacher.component';
import {ListStudentComponent} from './view/student/list-student/list-student.component';
import {AddStudentComponent} from './view/student/add-student/add-student.component';
import {ListClassComponent} from './view/class/list-class/list-class.component';
import {AddClassComponent} from './view/class/add-class/add-class.component';
import {ListRoomComponent} from './view/room/list-room/list-room.component';
import {AddSubjectComponent} from './view/subject/add-subject/add-subject.component';
import {ListSubjectComponent} from './view/subject/list-subject/list-subject.component';
import {LoginComponent} from './account/login/login.component';
import {EditClassComponent} from './view/class/edit-class/edit-class.component';
import {EditStudentComponent} from './view/student/edit-student/edit-student.component';
import {ListNoteComponent} from './view/note/list-note/list-note.component';
import {EditTeacherComponent} from './view/teacher/edit-teacher/edit-teacher.component';
import {DashboardComponent} from './view/dashboard/dashboard.component';
import {GuardService} from './service/guard.service';
import {AddNewsComponent} from './view/news/add-news/add-news.component';
import {EditNewsComponent} from './view/news/edit-news/edit-news.component';
import {ListNewsComponent} from './view/news/list-news/list-news.component';
import {AddClassStudentComponent} from './view/class/add-class-student/add-class-student.component';
import {AddClassSlotComponent} from './view/class/add-class-slot/add-class-slot.component';
import {EditSubjectComponent} from './view/subject/edit-subject/edit-subject.component';
import {AddScheduleComponent} from './view/schedule/add-schedule/add-schedule.component';
import {EditScheduleComponent} from './view/schedule/edit-schedule/edit-schedule.component';
import {ListScheduleComponent} from './view/schedule/list-schedule/list-schedule.component';
import {ListAttendentComponent} from './view/schedule/list-attendent/list-attendent.component';
import {IndexComponent} from './view/homeUniversity/index/index.component';
import {AboutComponent} from './view/homeUniversity/about/about.component';
import {NewsComponent} from './view/homeUniversity/news/news.component';
import {ContactComponent} from './view/homeUniversity/contact/contact.component';
import {ListEmployeeComponent} from './view/employee/list-employee/list-employee.component';
import {EditEmployeeComponent} from './view/employee/edit-employee/edit-employee.component';
import {AddEmployeeComponent} from './view/employee/add-employee/add-employee.component';
import {ListAdminComponent} from './view/admin/list-admin/list-admin.component';
import {EditAdminComponent} from './view/admin/edit-admin/edit-admin.component';
import {AddAdminComponent} from './view/admin/add-admin/add-admin.component';

const routes: Routes = [

  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {path: '', component: DashboardComponent, canActivate: [GuardService]},
  {path: 'listTeacher', component: ListTeacherComponent, canActivate: [GuardService]},
  {path: 'addTeacher', component: AddTeacherComponent, canActivate: [GuardService]},
  {path: 'listStudent', component: ListStudentComponent, canActivate: [GuardService]},
  {path: 'addStudent', component: AddStudentComponent, canActivate: [GuardService]},
  {path: 'editStudent', component: EditStudentComponent, canActivate: [GuardService]},
  {path: 'listClass', component: ListClassComponent, canActivate: [GuardService]},
  {path: 'addClass', component: AddClassComponent, canActivate: [GuardService]},
  {path: 'editClass', component: EditClassComponent, canActivate: [GuardService]},
  {path: 'listRoom', component: ListRoomComponent, canActivate: [GuardService]},
  {path: 'note', component: ListNoteComponent, canActivate: [GuardService]},
  {path: 'addSubject', component: AddSubjectComponent, canActivate: [GuardService]},
  {path: 'listSubject', component: ListSubjectComponent, canActivate: [GuardService]},
  {path: 'editSubject', component: EditSubjectComponent, canActivate: [GuardService]},
  {path: 'editTeacher', component: EditTeacherComponent, canActivate: [GuardService]},
  {path: 'addNews', component: AddNewsComponent, canActivate: [GuardService]},
  {path: 'listNews', component: ListNewsComponent, canActivate: [GuardService]},
  {path: 'editNews', component: EditNewsComponent, canActivate: [GuardService]},
  {path: 'addClassStudent', component: AddClassStudentComponent, canActivate: [GuardService]},
  {path: 'addClassSlot', component: AddClassSlotComponent, canActivate: [GuardService]},
  {path: 'addSchedule', component: AddScheduleComponent, canActivate: [GuardService]},
  {path: 'editSchedule', component: EditScheduleComponent, canActivate: [GuardService]},
  {path: 'listSchedule', component: ListScheduleComponent, canActivate: [GuardService]},
  {path: 'listAttendent', component: ListAttendentComponent, canActivate: [GuardService]},
  {path: 'homeUniversity', component: IndexComponent, canActivate: [GuardService]},
  {path: 'about', component: AboutComponent, canActivate: [GuardService]},
  {path: 'newsUniversity', component: NewsComponent, canActivate: [GuardService]},
  {path: 'contact', component: ContactComponent, canActivate: [GuardService]},
  {path: 'listEmployee', component: ListEmployeeComponent, canActivate: [GuardService]},
  {path: 'editEmployee', component: EditEmployeeComponent, canActivate: [GuardService]},
  {path: 'addEmployee', component: AddEmployeeComponent, canActivate: [GuardService]},
  {path: 'listAdmin', component: ListAdminComponent, canActivate: [GuardService]},
  {path: 'editAdmin', component: EditAdminComponent, canActivate: [GuardService]},
  {path: 'addAdmin', component: AddAdminComponent, canActivate: [GuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
