export class Student {
  'id': string;
  'fullName': string;
  'phone': string;
  'dob': string;
  'address': string;
  'image': string;
  'parentPhone': string;
  'idCard': string;
  'gender': string;
  'rollNumber': string;
  'status': string;
  'startYear': string;
  'endYear': string;
}
