export class Teacher {
  'id': string;
  'fullName': string;
  'phone': string;
  'dob': string;
  'image': string;
  'address': string;
  'idCard': string;
  'gender': string;
  'status': string;
  'account_id': string;
}
