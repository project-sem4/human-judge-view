export class Attendent {
  public id: string;
  public studentId: string;
  public studentName: string;
  public scheduleId: string;
  public slotId: string;
  public slotName: string;
  public status: string;
  public createdA: string;
  public updatedAt: string;
  public deletedAt: string;
}
