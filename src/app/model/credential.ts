export class Credential {
  'accessToken': string;
  'tokenExpiredAt': string;
  'createdAt': string;
}
