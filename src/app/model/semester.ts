export class Semester {
  'id': string;
  'name': string;
  'status': string;
  'createdAt': string;
  'updateAt': string;
}
