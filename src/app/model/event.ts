export class Event {
  id: string;
  // tslint:disable-next-line:variable-name
  start_date: string;
  // tslint:disable-next-line:variable-name
  end_date: string;
  text: string;
  status: string;
  color: string;
}
