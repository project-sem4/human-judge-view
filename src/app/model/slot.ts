export class Slot {
  id: string;
  name: string;
  startTime: string;
  endTime: string;
  status: string;
}
