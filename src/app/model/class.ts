export class Class {
  'id': string;
  'name': string;
  'roomName': string;
  'room_id': string;
  'start_time': string;
  'end_time': string;
  'createdAt': string;
  'updatedAt': string;
  'deletedAt': string;
  'status': string;
}
