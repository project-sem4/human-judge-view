export class Subject {
  'id': string;
  'code': string;
  'period': string;
  'semesterId': string;
  'nameSemester': string;
  'status': string;
  'subject': string;
  'createdAt': string;
  'updatedAt': string;
  'deletedAt': string;
}
