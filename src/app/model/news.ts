export class News {
  id: string;
  title: string;
  description: string;
  categoryId: string;
  categoryName: string;
  content: string;
  adminId: string;
  adminName: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
}
