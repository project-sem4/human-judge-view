export class Admin {
  id: string;
  fullName: string;
  phone: string;
  dob: string;
  address: string;
  status: string;
}
