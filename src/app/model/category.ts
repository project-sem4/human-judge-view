export class Category {
  id: string;
  name: string;
  adminId: string;
  adminName: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
}
