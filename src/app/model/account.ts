export class Account {
  public avatar: any;
  public createdAt: string;
  public deletedAt: string;
  public email: string;
  public id: any;
  public password: any;
  public role: string;
  public status: string;
  public username: string;
}
