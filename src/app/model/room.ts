export class Room {
  'id': string;
  'name': string;
  'createdAt': string;
  'updatedAt': string;
  'deletedAt': string;
  'status': string;
  'class_id': string;
}
