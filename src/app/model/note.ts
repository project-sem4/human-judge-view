export class Note {
  'id': string;
  'nameStudent': string;
  'nameTeacher': string;
  'note': string;
  'status': string;
  'createdAt': string;
  'updatedAt': string;
  'deletedAt': string;
}
