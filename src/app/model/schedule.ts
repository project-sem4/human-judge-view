export class Schedule {
  public id: number;
  public  clazzId: string;
  public  clazzName: string;
  public  subjectId: string;
  public  subjectName: string;
  public  roomId: string;
  public  roomName: string;
  public slotId: string;
  public  slotName: string;
  public  employeeId: string;
  public  employeeName: string;
  public  dayTypeId: string;
  public  dayTypeName: string;
  public  teacherId: string;
  public  teacherName: string;
  public  status: string;
  public  date: string;
  public startTime: string;
  public endTime: string;
}
