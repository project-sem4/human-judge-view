import {Component, OnInit} from '@angular/core';

import {LoginService} from '../../service/login.service';

import {Router} from '@angular/router';
import {Login} from '../../model/login';
import {Account} from '../../model/account';
import {Credential} from '../../model/credential';
import {HttpHeaders} from '@angular/common/http';
import {Admin} from '../../model/admin';
import {Employee} from '../../model/employee';
import {Student} from '../../model/student';
import {Teacher} from '../../model/teacher';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login: Login = new Login();

  account: Account = new Account();
  account1: Account = new Account();
  credential: Credential = new Credential();
  admin: Admin = new Admin();
  employee: Employee = new Employee();
  student: Student = new Student();
  teacher: Teacher = new Teacher();

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit() {
  }

  submit() {
    this.loginService.login(this.login).subscribe(res => {
      this.account = res.accountDTO;
      if (this.account.role === 'Admin') {
        this.admin = res.adminDTO;
        localStorage.setItem('accountDetail', JSON.stringify(this.admin));
      } else if (this.account.role === 'Manager') {
        this.employee = res.employeeDTO;
        localStorage.setItem('accountDetail', JSON.stringify(this.employee));
      } else if (this.account.role === 'Student') {
        this.student = res.studentDTO;
        localStorage.setItem('accountDetail', JSON.stringify(this.student));
      } else if (this.account.role === 'Teacher') {
        this.teacher = res.teacherDTO;
        localStorage.setItem('accountDetail', JSON.stringify(this.teacher));
      }
      this.credential = res.credentialDTO;
      localStorage.setItem('accountCurrent', JSON.stringify(this.account));
      localStorage.setItem('credentialCurrent', JSON.stringify(this.credential));
      const accountCurrent = localStorage.getItem('accountCurrent');
      this.account1 = JSON.parse(accountCurrent);
      this.router.navigate(['']);
    }, error => {
      alert('loi');
    });

  }

}
