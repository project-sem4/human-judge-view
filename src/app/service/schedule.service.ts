import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Schedule} from '../model/schedule';
import {Credential} from '../model/credential';
import {Employee} from '../model/employee';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));
  schedule: Schedule = new Schedule();

  constructor(private http: HttpClient) {
  }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + this.credential.accessToken
    })
  };
  private UrlSchedules = API_URL + '/schedules';

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }


  list(teacherId: any): Observable<any> {
    return this.http.get(this.UrlSchedules + '/getScheduleByTeacher?teacherId=' + teacherId, this.httpOptions);
  }

  listByClass(classId: any): Observable<any> {
    return this.http.get(this.UrlSchedules + '/getScheduleByClass?classId=' + classId, this.httpOptions);
  }

  delete(schduleId: any): Observable<any> {
    return this.http.delete(this.UrlSchedules + '/' + schduleId, this.httpOptions);
  }

  activeSchedule(scheduleId: any): Observable<any> {
    this.schedule.id = scheduleId;
    return this.http.put(this.UrlSchedules + '/activeSchedule', this.schedule, this.httpOptions);
  }

  edit(schduleId: any, schedule: Schedule): Observable<any> {
    return this.http.put(this.UrlSchedules + '/' + schduleId, schedule, this.httpOptions);
  }

  create(schedule: any): Observable<any> {
    console.log(schedule);
    return this.http.post(this.UrlSchedules, schedule, this.httpOptions);
  }
}
