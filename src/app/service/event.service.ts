import {Injectable} from '@angular/core';
import {Event} from '../model/event';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {throwError} from 'rxjs';
import {Credential} from '../model/credential';
import {Account} from '../model/account';
import {Teacher} from '../model/teacher';
import {Student} from '../model/student';


const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class EventService {
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));
  account: Account = JSON.parse(localStorage.getItem('accountCurrent'));
  teacher: Teacher = new Teacher();
  private eventUrl = 'http';
  student: Student = new Student();

  constructor(private http: HttpClient) {
  }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + this.credential.accessToken
    })
  };

  private UrlSchedule = API_URL + '/schedules';

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  get(): Promise<any> {
    if (this.account.role === 'Admin' || this.account.role === 'Manager') {
      // @ts-ignore
      return this.http.get(this.UrlSchedule, this.httpOptions)
        .toPromise()
        .catch(this.handleError);
    } else if (this.account.role === 'Teacher') {
      this.teacher = JSON.parse(localStorage.getItem('accountDetail'));
      // @ts-ignore
      return this.http.get(this.UrlSchedule + '/getScheduleByTeacher' + '?teacherId=' + this.teacher.id, this.httpOptions)
        .toPromise()
        .catch(this.handleError);
    } else if (this.account.role === 'Student') {
      this.student = JSON.parse(localStorage.getItem('accountDetail'));
      // @ts-ignore
      return this.http.get(this.UrlSchedule + '/getScheduleByStudent' + '?studentId=' + this.student.id, this.httpOptions)
        .toPromise()
        .catch(this.handleError);
    }
  }
}
