import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Credential} from '../model/credential';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class DayTypeService {
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));

  private UrlDayType = API_URL + '/dayType';

  constructor(private http: HttpClient) {
  }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + this.credential.accessToken
    })
  };

  list(): Observable<any> {
    return this.http.get(this.UrlDayType, this.httpOptions);
  }
}
