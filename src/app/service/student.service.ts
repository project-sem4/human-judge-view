import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {environment} from '../../environments/environment';
import {Student} from '../model/student';
import {catchError, retry, tap} from 'rxjs/operators';
import {Class} from '../model/class';
import {Credential} from '../model/credential';
import {log} from 'util';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));

  constructor(private http: HttpClient) {
  }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      authorization: 'Bearer' + this.credential.accessToken
    })
  };

  private UrlStudent = API_URL + '/students';
  private UrlAccount = API_URL + '/accounts/student';

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  create(student: any): Observable<any> {
    return this.http.post(this.UrlAccount, student, this.httpOptions);
  }

  getSudents(): Observable<any> {
    return this.http.get(this.UrlStudent, this.httpOptions).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }
  getSudentsByClass(classId: any): Observable<any> {
    return this.http.get(this.UrlStudent + '/getStudentInClass?clazzId=' + classId, this.httpOptions).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }
  update(id: string, value: Student): Observable<Student> {
    return this.http.put<Student>(this.UrlStudent + '/' + id, value, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  updateImage(studentId: any, value: Student): Observable<Student> {
    return this.http.put<Student>(this.UrlStudent + '/updateListImageForStudent' +
      '?studentId=' + studentId, value, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  delete(id: any): Observable<any> {
    return this.http.delete<Class>(this.UrlStudent + '/' + id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  getStudent(id: any): Observable<any> {
    return this.http.get(this.UrlStudent + '/' + id, this.httpOptions);
  }
}
