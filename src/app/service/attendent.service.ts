import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Credential} from '../model/credential';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AttendentService {
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));

  constructor(private http: HttpClient) {
  }


// Http Options
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + this.credential.accessToken
    })
  };

  private UrlCLass = API_URL + '/attendances';

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  getAttendentByScheduleId(scheduleId: any): Observable<any> {
    return this.http.get(this.UrlCLass + '/getAttendanceBySchedule?scheduleId=' + scheduleId, this.httpOptions);
  }

  getAttendentByClass(classId: any): Observable<any> {
    return this.http.get(this.UrlCLass + '/getScheduleByClass?classId=' + classId, this.httpOptions);
  }

  getAttendentByStudentId(studentId: any): Observable<any> {
    return this.http.get(this.UrlCLass + '/getAttendanceByStudent?studentId=' + studentId, this.httpOptions);
  }

  getAttendentByStudentIdAndScheduleId(studentId: any, scheduleId: any): Observable<any> {
    // tslint:disable-next-line:max-line-length
    return this.http.get(this.UrlCLass + '/getAttendanceByStudentAndSchedule?studentId=' + studentId + '&scheduleId=' + scheduleId, this.httpOptions);
  }

  updateAttendent(attendent: any, attendentId: any): Observable<any> {
    return this.http.put(this.UrlCLass + '/' + attendentId, attendent, this.httpOptions);
  }

}
