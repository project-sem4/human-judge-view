import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map, retry} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Class} from '../model/class';
import {Credential} from '../model/credential';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ClassService {
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));

  constructor(private http: HttpClient) {
  }


// Http Options
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + this.credential.accessToken
    })
  };

  private UrlCLass = API_URL + '/clazz';

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }


  list(): Observable<any> {
    return this.http.get(this.UrlCLass, this.httpOptions);
  }

  getClassByStudent(): Observable<any> {
    return this.http.get(this.UrlCLass, this.httpOptions);
  }
  getClassByStudentId(studentId: any): Observable<any> {
    return this.http.get(this.UrlCLass + '/getClassByStudent?studentId=' + studentId, this.httpOptions);
  }


  create(clazz: any, roomId: any): Observable<any> {
    return this.http.post(this.UrlCLass + '?roomId=' + roomId, clazz, this.httpOptions);
  }

  addClassStudent(clazz: any, studentList: any, list: any, classId: any): Observable<any> {
    return this.http.post(this.UrlCLass + '/arrangeclass' + '?studentList=' + studentList + '&classId=' + classId, clazz, this.httpOptions);
  }

  addClassSlot(clazz: any, slotList: any, list: any, classId: any): Observable<any> {
    return this.http.post(this.UrlCLass + '/arrangeSlot' + '?slotList=' + slotList + '&classId=' + classId, clazz, this.httpOptions);
  }

  update(id: string, roomId: any, value: Class): Observable<Class> {
    return this.http.put<Class>(this.UrlCLass + '/' + id + '?roomId=' + roomId, value, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  delete(id: any): Observable<any> {
    return this.http.delete<Class>(this.UrlCLass + '/' + id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  getClass(id: any): Observable<any> {
    return this.http.get(this.UrlCLass + '/' + id, this.httpOptions);
  }

  search(name: any): Observable<any> {
    return this.http.get(this.UrlCLass + '/data/' + name, this.httpOptions);
  }
}
