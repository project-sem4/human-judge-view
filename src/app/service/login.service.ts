import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Login} from '../model/login';
import {Observable} from 'rxjs';
import {Credential} from '../model/credential';
import {log} from 'util';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {
  }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'content-type': 'application/json'
    })
  };

  private UrlLogin = 'https://spring-boot-university.herokuapp.com/login';

  login(login: Login): Observable<any> {
    // const urlSearchParams = new URLSearchParams();
    // const body = urlSearchParams.toString();
    return this.http.post(this.UrlLogin, login, this.httpOptions);
  }

  isUserLoggedIn() {
    const user = sessionStorage.getItem('role');
    return !(user === null);
  }
}
