import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Credential} from '../model/credential';
import {Sms} from '../model/sms';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class SmsService {
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));

  private convertParentPhone;
  private UrlSMS = API_URL + '/sms';

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + this.credential.accessToken
    })
  };

  constructor(private http: HttpClient) {
  }

  create(value: Sms): Observable<any> {
    // phần %2b84 = +84
    // this.convertParentPhone = '%2b84' + listParentPhone;
    // @ts-ignore
    return this.http.post(this.UrlSMS + '/sendSMS', value, this.httpOptions);
  }
}
