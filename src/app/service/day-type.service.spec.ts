import { TestBed } from '@angular/core/testing';

import { DayTypeService } from './day-type.service';

describe('DayTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DayTypeService = TestBed.get(DayTypeService);
    expect(service).toBeTruthy();
  });
});
