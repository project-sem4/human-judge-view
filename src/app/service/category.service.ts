import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Credential} from '../model/credential';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));

  private UrlCategory = API_URL + '/category';

  constructor(private http: HttpClient) {
  }

// Http Options
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + this.credential.accessToken
    })
  };

  list(): Observable<any> {
    return this.http.get(this.UrlCategory, this.httpOptions);
  }
}
