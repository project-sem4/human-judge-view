import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from '../../environments/environment';
import {catchError, retry} from 'rxjs/operators';
import {News} from '../model/news';
import {Credential} from '../model/credential';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + this.credential.accessToken
    })
  };

  private UrlNews = API_URL + '/news';

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  create(news: any): Observable<any> {
    return this.http.post(this.UrlNews, news, this.httpOptions);
  }

  getNews(): Observable<any> {
    return this.http.get(this.UrlNews, this.httpOptions).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  update(id: string, value: News): Observable<News> {
    return this.http.put<News>(this.UrlNews + '/' + id, value, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  delete(id: any): Observable<any> {
    return this.http.delete<News>(this.UrlNews + '/' + id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  getNew(id: any): Observable<any> {
    return this.http.get(this.UrlNews + '/' + id, this.httpOptions);
  }

  getNewsByCategory(id: any): Observable<any> {
    return this.http.get(this.UrlNews + '/getArticleByCategory' + '?catId=' + id, this.httpOptions).pipe(
      retry(2),
      catchError(this.handleError)
    );
  }

  search(name: any): Observable<any> {
    return this.http.get(this.UrlNews + '/data/' + name, this.httpOptions);
  }
}
