import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(private route: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const accountCurrent = localStorage.getItem('accountCurrent');
    if (accountCurrent) {
      return true;
      this.route.navigate(['/']);
    } else {
      this.route.navigate(['login']);
      return false;
    }
  }
}
