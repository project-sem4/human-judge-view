import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Credential} from '../model/credential';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class SlotService {
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));

  private UrlSlot = API_URL + '/slot';
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + this.credential.accessToken
    })
  };

  constructor(private http: HttpClient) {
  }

  list(): Observable<any> {
    return this.http.get(this.UrlSlot, this.httpOptions);
  }
}
