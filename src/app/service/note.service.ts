import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from '../../environments/environment';
import {Room} from '../model/room';
import {catchError, retry} from 'rxjs/operators';
import {Note} from '../model/note';
import {Class} from '../model/class';
import {Credential} from '../model/credential';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class NoteService {
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));

  constructor(private http: HttpClient) {
  }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + this.credential.accessToken
    })
  };

  private UrlNote = API_URL + '/note';

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  list(): Observable<any> {
    return this.http.get(this.UrlNote, this.httpOptions);
  }

  detail(id: any): Observable<any> {
    return this.http.get(this.UrlNote + '/' + id, this.httpOptions);
  }

  getNote(id: any): Observable<any> {
    return this.http.get(this.UrlNote + '/' + id, this.httpOptions);
  }

  create(studentId: any, techerId: any, note: any): Observable<any> {
    return this.http.post(this.UrlNote + '?studentId=' + studentId + '&teacherId=' + techerId, note, this.httpOptions);
  }

  update(id: any, note: any): Observable<any> {
    return this.http.put(this.UrlNote + '/' + id, note, this.httpOptions);
  }

  delete(id: any): Observable<any> {
    return this.http.delete<Note>(this.UrlNote + '/' + id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }
}
