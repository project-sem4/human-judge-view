import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from '../../environments/environment';
import {Credential} from '../model/credential';
import {Teacher} from '../model/teacher';
import {catchError, retry} from 'rxjs/operators';
import {Employee} from '../model/employee';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  credential: Credential = JSON.parse(localStorage.getItem('credentialCurrent'));

  private UrlEmployee = API_URL + '/employees';
  private UrlAccount = API_URL + '/accounts/employee';

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: 'Bearer ' + this.credential.accessToken
    })
  };

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  constructor(private http: HttpClient) {
  }

  list(): Observable<any> {
    return this.http.get(this.UrlEmployee, this.httpOptions);
  }

  create(employee: any): Observable<any> {
    return this.http.post(this.UrlAccount, employee, this.httpOptions);
  }

  update(id: any, employee: any): Observable<any> {
    return this.http.put(this.UrlEmployee + '/' + id, employee, this.httpOptions);
  }

  delete(id: any): Observable<any> {
    return this.http.delete<Employee>(this.UrlEmployee + '/' + id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  getEmployee(id: any): Observable<any> {
    return this.http.get(this.UrlEmployee + '/' + id, this.httpOptions);
  }
}
