import { TestBed } from '@angular/core/testing';

import { AttendentService } from './attendent.service';

describe('AttendentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AttendentService = TestBed.get(AttendentService);
    expect(service).toBeTruthy();
  });
});
